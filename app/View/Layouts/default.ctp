<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'CRC - LEVANTE SUR ');
?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->Html->charset(); ?> <!-- para que admita los caracteres de español -->
		
		<title>
			<?php 
				// $cakeDescription - Es una variable que uso de vez en cuando en el diseño
				// $title_for_layout - Es el título de la  página
				echo $cakeDescription . '...' . $title_for_layout; 
			?>
		</title>

		<?php
			echo $this->Html->meta('icon');

			// A continuación le decimos la hoja de estilos que va a usar y se encuentra en el directorio ... app/webroot/css
			// Hay que tener en cuenta que el nombre del fichero .css no lleva puesto la terminación .css pero que si que
			// existe con esa terminación en el directorio anteriormente comentado
			echo $this->Html->css('cake.generic'); 

			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
	</head>

	<body>
		<div id="container">
		
			<div id="header">
				<h1>
					<?php 
						echo $this->Html->link($cakeDescription, 'http://www.jw.org');
						// echo 'CRC - LEVANTE SUR ' .
 								 // $this->Html->link(   $this->Html->image('jw.png', array('alt' => $cakeDescription, 'border' => '0')),
																 // 'http://www.jw.org',
																 // array('target' => '_blank', 'escape' => false)   );
					?>
				</h1>
			</div>
			
			<div id="content">
				<?php 
					// Sirve para decirle donde van a aparecer los mensajes flash
					echo $this->Session->flash(); 
				?> 

				<?php 
					// Sirve para decirle donde van a aparecer los contenidos para el content
					echo $this->fetch('content'); 
				?>
			</div>
			
			<div id="footer">
				<?php 
					echo $this->Html->link(   $this->Html->image('jw.png', array('alt' => $cakeDescription, 'border' => '0')),
															 'http://www.jw.org',
															 array('target' => '_blank', 'escape' => false)   );
				?>
			</div>
			
		</div>
		
		<?php 
			// Con la línea de abajo vemos el volcado de las SQL que se vayan realizando
			
			// echo $this->element('sql_dump'); 
		?>
		
	</body>
</html>
