<div class="estates form">
<?php echo $this->Form->create('Estate'); ?>
	<fieldset>
		<legend><?php echo __('Add Estate'); ?></legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('estate_type_id');
		echo $this->Form->input('Congregation');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Estates'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Estate Types'), array('controller' => 'estate_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Estate Type'), array('controller' => 'estate_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Projects'), array('controller' => 'projects', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Project'), array('controller' => 'projects', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Congregations'), array('controller' => 'congregations', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Congregation'), array('controller' => 'congregations', 'action' => 'add')); ?> </li>
	</ul>
</div>
