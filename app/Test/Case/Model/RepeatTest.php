<?php
App::uses('Repeat', 'Model');

/**
 * Repeat Test Case
 *
 */
class RepeatTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.repeat',
		'app.task',
		'app.taskstype'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Repeat = ClassRegistry::init('Repeat');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Repeat);

		parent::tearDown();
	}

}
