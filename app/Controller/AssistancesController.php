<?php
App::uses('AppController', 'Controller');
/**
 * Assistances Controller
 *
 * @property Assistance $Assistance
 */
class AssistancesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Assistance->recursive = 0;
		$this->set('assistances', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Assistance->exists($id)) {
			throw new NotFoundException(__('Invalid assistance'));
		}
		$options = array('conditions' => array('Assistance.' . $this->Assistance->primaryKey => $id));
		$this->set('assistance', $this->Assistance->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Assistance->create();
			if ($this->Assistance->save($this->request->data)) {
				$this->Session->setFlash(__('The assistance has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The assistance could not be saved. Please, try again.'));
			}
		}
		$users = $this->Assistance->User->find('list');
		$projects = $this->Assistance->Project->find('list');
		$this->set(compact('users', 'projects'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Assistance->exists($id)) {
			throw new NotFoundException(__('Invalid assistance'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Assistance->save($this->request->data)) {
				$this->Session->setFlash(__('The assistance has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The assistance could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Assistance.' . $this->Assistance->primaryKey => $id));
			$this->request->data = $this->Assistance->find('first', $options);
		}
		$users = $this->Assistance->User->find('list');
		$projects = $this->Assistance->Project->find('list');
		$this->set(compact('users', 'projects'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Assistance->id = $id;
		if (!$this->Assistance->exists()) {
			throw new NotFoundException(__('Invalid assistance'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Assistance->delete()) {
			$this->Session->setFlash(__('Assistance deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Assistance was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
